package ma.digiup.assignement.mapper;

import ma.digiup.assignement.domain.Transfer;
import ma.digiup.assignement.dto.TransferDto;

public class TransferMapper {

    public static TransferDto map(Transfer transfer) {
        TransferDto transferDto = new TransferDto();
        transferDto.setNrCompteEmetteur(transfer.getCompteEmetteur().getNrCompte());
        transferDto.setDate(transfer.getDateExecution());
        transferDto.setMotif(transfer.getMotifTransfer());

        return transferDto;

    }
}
