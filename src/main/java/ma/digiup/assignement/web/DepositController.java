package ma.digiup.assignement.web;

import ma.digiup.assignement.domain.Compte;
import ma.digiup.assignement.exceptions.CompteNonExistantException;
import ma.digiup.assignement.exceptions.TransactionException;
import ma.digiup.assignement.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("/deposit")
public class DepositController {
    @Autowired
    private DepositService depositService;

    public void setDepositService(DepositService depositService) {
        this.depositService = depositService;
    }

    @PatchMapping("/deposit")
    public ResponseEntity<Compte> Deposit(@RequestParam String Rib , @RequestParam Double Montant ){
        try{
            return new ResponseEntity<>(depositService.deposit(Rib,Montant), HttpStatus.OK);
        }catch (CompteNonExistantException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } catch (TransactionException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

    }
}
