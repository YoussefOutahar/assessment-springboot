package ma.digiup.assignement.service;

import ma.digiup.assignement.domain.Compte;
import ma.digiup.assignement.exceptions.CompteNonExistantException;
import ma.digiup.assignement.exceptions.TransactionException;
import ma.digiup.assignement.repository.CompteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class DepositService {

    public static final int MAX_AMOUNT = 10000;
    Logger LOGGER = LoggerFactory.getLogger(DepositService.class);

    @Autowired
    public CompteRepository compteRepository;

    public Compte deposit(String rib, Double montant) throws CompteNonExistantException, TransactionException {

        LOGGER.info("Dépôt de {} sur le compte {}", montant, rib);

        Compte compte = compteRepository.findCompteByRib(rib);

        if (compte == null) {
            throw new CompteNonExistantException("Compte inexistant");
        }
        if (compte.getSolde().compareTo(BigDecimal.valueOf(montant)) < 0) {
            throw new TransactionException("Solde insuffisant");
        }

        if (montant > MAX_AMOUNT) {
            throw new TransactionException("Montant maximal dépassé");
        }

        BigDecimal nouveauSolde = compte.getSolde().add(BigDecimal.valueOf(montant));
        compte.setSolde(nouveauSolde);

        return compteRepository.save(compte);
    }
}
