package ma.digiup.assignement.domain;

import lombok.Getter;
import lombok.Setter;
import ma.digiup.assignement.domain.util.EventType;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "AUDIT")
public class AuditTransfer {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 100)
  private String message;

  @Enumerated(EnumType.STRING)
  private EventType eventType;

}
