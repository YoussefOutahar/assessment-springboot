package ma.digiup.assignement.web;

import ma.digiup.assignement.domain.Compte;
import ma.digiup.assignement.exceptions.CompteNonExistantException;
import ma.digiup.assignement.exceptions.TransactionException;
import ma.digiup.assignement.service.DepositService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.transaction.Transactional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class DepositControllerTest {
    @Mock
    private DepositService depositService;

    @InjectMocks
    private DepositController depositController;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(depositController).build();
    }

    @Test
    public void testDeposit_Success() throws Exception {
        when(depositService.deposit("123456", 100.0)).thenReturn(new Compte(/* create a Compte object here */));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/deposit")
                        .param("rib", "123456")
                        .param("montant", "100.0")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeposit_CompteNonExistantException() throws Exception {
        when(depositService.deposit("invalidRib", 100.0)).thenThrow(new CompteNonExistantException("Compte inexistant"));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/deposit")
                        .param("rib", "invalidRib")
                        .param("montant", "100.0")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeposit_TransactionException() throws Exception {
        when(depositService.deposit("123456", 100000.0)).thenThrow(new TransactionException("Montant maximal dépassé"));

        mockMvc.perform(MockMvcRequestBuilders.post("/api/deposit")
                        .param("rib", "123456")
                        .param("montant", "100000.0")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}
