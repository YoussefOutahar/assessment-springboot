package ma.digiup.assignement.repository;

import ma.digiup.assignement.domain.Transfer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class TransferRepositoryTest {

  @Autowired
  private TransferRepository transferRepository;

  @Test
  public void findAll() {
    Transfer transfer1 = createSampleTransfer();
    Transfer transfer2 = createSampleTransfer();

    transferRepository.save(transfer1);
    transferRepository.save(transfer2);
    List<Transfer> transfers = transferRepository.findAll();

    assertEquals(2, transfers.size());
  }

  @Test
  public void findOne() {
    Transfer transfer = createSampleTransfer();

    Transfer savedTransfer = transferRepository.save(transfer);
    Optional<Transfer> foundTransfer = transferRepository.findById(savedTransfer.getId());

    assertTrue(foundTransfer.isPresent());
    assertEquals(savedTransfer.getId(), foundTransfer.get().getId());
  }

  @Test
  public void save() {
    Transfer transfer = createSampleTransfer();

    Transfer savedTransfer = transferRepository.save(transfer);

    assertNotNull(savedTransfer.getId());
  }

  @Test
  public void delete() {
    Transfer transfer = createSampleTransfer();
    Transfer savedTransfer = transferRepository.save(transfer);

    transferRepository.delete(savedTransfer);

    assertFalse(transferRepository.findById(savedTransfer.getId()).isPresent());
  }

  private Transfer createSampleTransfer() {
    Transfer transfer = new Transfer();
    transfer.setMontantTransfer(new BigDecimal("100.00"));
    transfer.setDateExecution(new Date());
    return transfer;
  }
}